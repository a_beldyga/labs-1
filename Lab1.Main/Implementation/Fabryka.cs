﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Fabryka: IJablko, IMaszyna
    {
        public Fabryka() { }
        public string Technika="fermentacja";

        public string Przetwory()
        {
            return "W fabryce z owoców robi się soki";
        }

        public int Godziny()
        {
            return 6;
        }

        public int PodajIlosc()
        {
            return 5;
        }

        string IOwoc.Przetwory()
        {
            return this.Technika + " to technika do wytwarzania owoców";
        }

        string IJablko.Przetwory()
        {
            return "Przetwarzane są też jablka";
        }
        string IMaszyna.Przetwory()
        {
            return "Przetwory tworzone są w maszynach";
        }
    }
}
