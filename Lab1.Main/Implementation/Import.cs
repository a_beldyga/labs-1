﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Import: IPomarancza
    {
        public Import() { }


        public string Sezon()
        {
            return "najlepsze pomarańcze są w październiku";
        }

        public string Przetwory()
        {
            return "Z pomarańczy można zrobić pyszny dżem";
        }
    }
}
