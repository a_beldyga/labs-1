﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Sad: IJablko
    {
        public int ilosc;

        public Sad() { }
        
        public Sad(int ilosc)
        {
            this.ilosc = ilosc;
        }

        
        public int PodajIlosc()
        {
            return this.ilosc*3;
        }

        public string Przetwory()
        {
            return "Z jabłek można zrobić sok";
        }
    }
}
