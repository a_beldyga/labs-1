﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IOwoc);
        
        public static Type ISub1 = typeof(IJablko);
        public static Type Impl1 = typeof(Sad);
        
        public static Type ISub2 = typeof(IPomarancza);
        public static Type Impl2 = typeof(Import);
        
        
        public static string baseMethod = "Przetwory";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "PodajIlosc";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Sezon";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "FactoryMethod";
        public static string collectionConsumerMethod = "ConsumerMethod";

        #endregion

        #region P3

        public static Type IOther = typeof(IMaszyna);
        public static Type Impl3 = typeof(Fabryka);

        public static string otherCommonMethod = "Przetwory";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
