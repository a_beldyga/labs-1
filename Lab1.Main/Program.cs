﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {

        
       public static void Main(string[] args)
        {
            Program program = new Program();
            var kolekcja = program.FactoryMethod();
            program.ConsumerMethod(kolekcja);
               

            var impl3 = new Fabryka();
       //     string n1 = ((IOther)impl3).Napis();
         //   string n2 = ((IJablko)impl3).Napis;
            string n3 = impl3.Przetwory();

           // Console.WriteLine(n1 + " " + n2 + " ");
            Console.ReadKey();

        }

       public IList<IOwoc> FactoryMethod()
        {
            var impl1 = new Sad();
            var impl2 = new Import();
            var impl3 = new Import();

            return new List<IOwoc> { impl1, impl2, impl3 };
        }

        public void ConsumerMethod(IList<IOwoc> kolekcja)
        {
            foreach (var owoc in kolekcja)
            {
                Console.WriteLine(owoc.Przetwory());
  
            }
        }
    }
}
